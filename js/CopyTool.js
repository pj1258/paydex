/*
 * CopyTool||v.0.1
 * APP复制整合插件
 * 注意W_copybtn必须位于需要复制的内容同级后面.siblings()
 * */
function useCopyTool(){
	mui(document).on('tap','.W_copybtn',function(e){
		var copytxt = this.previousElementSibling.innerText;
		if(plus.os.name=='Android'){
			//获取剪切板ANDROID
			var Context = plus.android.importClass("android.content.Context");
	        var main = plus.android.runtimeMainActivity();
	        var clip = main.getSystemService(Context.CLIPBOARD_SERVICE);
	        // 设置文本内容:
	        plus.android.invoke(clip,"setText",copytxt);
			mui.toast(vm.lang.COM_yifuzhi);
	      	
	    }
	    if(plus.os.name=='iOS'){
	    	//获取剪切板IOS
			var UIPasteboard = plus.ios.importClass("UIPasteboard");
			var generalPasteboard = UIPasteboard.generalPasteboard();
			// 设置文本内容:
			generalPasteboard.setValueforPasteboardType(copytxt, "public.utf8-plain-text");
			mui.toast(vm.lang.COM_yifuzhi);
	    }
	})
}