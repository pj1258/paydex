
var APP = (function($, undefined) {
	
	var publicKey = '-----BEGIN PUBLIC KEY-----MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAsprm+5oc4gfMp7JuHmAXOBrONwWg7L7NnhHO2bZ2QbUrZkSmIoynYfacvGxGJvqj/LxGaulOVTdiMfZJakXQzUWCG42NOerlLlyN1799dPnvlbRhNURvw+eYjT2QFUrH8TdTlf+kEZxbV6mQQViXiYap02Sd3LVxogPL88KkyLrgkCQ9pkKZJQ5KDe+KIpL1peSkoL6frBDZZ4YPu1R/JG7i9+z+Z7ahluXQFTwrEPv+tgSvvLIVgiv9YeTBJwYmArkC1+qPeJXaK/2zX0ApV2eW04rRKCZjLFWlYB6ZUJaqAwiYqmcPu1YRhv9jxZ6eNHyfouQNkKKsIPV3325MF9zbKx5AvHc7Bbs8n7HMBnD0ntbNue4n7HSK9uRJQTw4iGESES2w0HKGT0s69eitqISDRKW3//qkWEtWQvHN6oYdGX1+1WhTXxl0Scr7Rh1RkwUf2Vog1emJhi4IgIdE3FssAvTF8KA1TCBGgYTGW5UyrpR2QWp2KBTgl7UG2p9PDHNGu0+iw0teqWIx32MyW2MD1DrA1+abnMxr4QY/ahtirivgC7dEWmg78luGhetA5UhFIJs0yHuXArgRQBd41CL1jKRtsfDOa03J0lGBe/OaNMup+aeZIlqUrtwW9562WlEMpVXSJlOHc96SCSeQiY/XFQDlDsSLge4ueqb4vUkCAwEAAQ==-----END PUBLIC KEY-----';
//window.addEventListener('resize', rem);
	var app = {
		
		debug: true,		// debug模式
		log: function(message) {
			if (this.debug) {
	      var now = new Date();
	      console.log("VPAY LOGS: " + now.toLocaleTimeString() + '.' + now.getMilliseconds() + '> ' + message);
	   }
		},
		wvconfig: {
			url: '',
			id: '',
			show:{
//				autoShow: false,
		  		aniShow: 'slide-in-right', //页面显示动画，默认为”slide-in-right“；
		  },
		  waiting:{
		    autoShow: false, //自动显示等待框，默认为true
		  },
		  styles: {
		  		scrollIndicator: 'none'
		  },
		  extras: {
				title: '',
			}
		},
		openWV: function(url, config) {
			// 检查网络链接，避免打开页面白屏
			if(window.plus) {
				if(plus.networkinfo.getCurrentType() === plus.networkinfo.CONNECTION_NONE) {
					mui.toast("网络连接失败");
					return false;
				}
			}
			var delay;
			if(mui.os.ios) {
				var delay = mui.os.ios ? 50 : 150;
				app.wvconfig.show.aniShow = 'pop-in';
			}
			config = config || {};
			config.url = url;
			config.id = config.id || url;
			config = $.extend(true, {}, app.wvconfig, config);
			var nowWV = $.openWindow(config);
			return nowWV;
		},
		openWVN: function(url, config) {
			// 检查网络链接，避免打开页面白屏
			if(window.plus) {
				if(plus.networkinfo.getCurrentType() === plus.networkinfo.CONNECTION_NONE) {
					mui.toast("网络连接失败");
					return false;
				}
			}
			var delay;
			if(mui.os.ios) {
				var delay = mui.os.ios ? 50 : 150;
				app.wvconfig.show.aniShow = 'pop-in';
			}
			config = config || {};
			config.url = url;
			config.id = config.id || url;
			config = $.extend(true, {}, app.wvconfig, config);
			delete config.styles.statusbar;
			var nowWV = $.openWindow(config);
			return nowWV;
		},
		/* 接口相关配置 */
		PHP_ROOT: 'http://api.matrixex.io/api',
//		PHP_ROOT: 'http://47.244.140.126:8883/api',
		PYTHON_ROOT: 'http://api.matrixex.io/v1/api/ecologychain',
//		PYTHON_ROOT: 'http://47.244.140.126:9001/v1/api/ecologychain',
		imgAddress:'http://vpc-coin.oss-cn-beijing.aliyuncs.com/',
		APP_SIGN:'HyCBHrg8cFJNNrXWyBBQw7sUww8EcEdM',
		rand_string:Math.random().toString(36).substr(2),
//		PHP_ROOT: 'https://api.unite-dex.com/api',	
		error: function(error){
			console.log("error_code:"+JSON.stringify(error.response))
			if (error.response) {
//				console.log(JSON.stringify(error.response.data));
//	      console.log(error);
				// 1. 401 Token过期
//				mui.toast(error.response.data.msg);
		
				if(error.response.code == 436) {
					
//					plus.storage.setItem('token','int')
//					plus.storage.setItem('tokenNum','0');
//					mui.fire(plus.webview.getWebviewById('pages/Finance/Finance.html'), "re-token");
//					mui.fire(plus.webview.getWebviewById('pages/User/User.html'), "re-token");
					 // 刷新打开login.html的页面，如需则进行监听
				
//					if(!plus.storage.getItem('launchFlag')) return false;
//					if(!plus.webview.getWebviewById('Login')){
//						var aniShow = 'slide-in-right'
//						if(plus.storage.getItem('hasCheckVersion')) {
//							aniShow = 'none'
//						}
//						var loginWV = APP.openWV('/pages/Login/Login.html', {
//							id: 'Login',					
//							show: {
//								autoShow: true,
//								aniShow: aniShow
//							},
//							extras: {
//								relaunch: true
//							}
//						});
//					}
//					setTimeout(function() {
//						loginWV.show('slide-in-right');
//					}, 30)
					return false;
			 	}else if(error.response.status == 401) {
//			 		APP.alert(vm.lang.Login_loginAnother,function(){
//			 			var allWV = plus.webview.all();
//						for(var k = 0, len = allWV.length; k < len; k++) {
//							if(allWV[k].getURL().indexOf('www/index.html') == -1 && allWV[k].getURL().indexOf('Login/Login.html') == -1 ){
//									allWV[k].getURL().href=allWV[k].getURL().href+"www/index.html"+"Login/Login.html"
//							     	allWV[k].reload();
//	
//							}																																	
//						}
//			 		})
			 	}else {
			 		return error.response.data;
			 	}
				
	      // The request was made and the server responded with a status code
	      // that falls out of the range of 2xx
	      
	    } else if (error.request) {
	      // The request was made but no response was received
	      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
	      // http.ClientRequest in node.js
//	      console.log(error.request);
	    } else {
	      // Something happened in setting up the request that triggered an Error
//	      console.log('Error', error.message);
	    }
//	    console.log(JSON.stringify(error.config));
			return {};
		},
		success: function(res) {
			if(res.data.code==436 || res.data.code==437){
					if(!plus.storage.getItem('launchFlag')) return false;
					if(!plus.webview.getWebviewById('enter')){
						var aniShow = 'slide-in-right'
					
						var loginWV = APP.openWV('/pages/login/enter.html', {
							id: 'enter',					
							show: {
								autoShow: true,
								aniShow: aniShow
							},
							extras: {
								relaunch: true
							}
						});
//					
					}

//					
					return false;
				
			}else if(res.data.code==433){
//				
					APP.alert(vm.lang.Login_loginAnother,function(){

						if(!plus.webview.getWebviewById('enter')){
							var aniShow = 'slide-in-right'
							
							var loginWV = APP.openWV('/pages/login/enter.html', {
								id: 'enter',		
								
								show: {
												
												
												
												
									autoShow: true,
									aniShow: aniShow
									
								},
								extras: {
									relaunch: true
								}
							});
//							setTimeout(function() {
//								loginWV.show('slide-in-right');
//							}, 30)
						}

			 		})
					return false;
//				}
				
			}else{
				return res.data;
			}
			
		},
		/**
		 * axios.get 
		 */
		get: function(url, data) {
			var token = plus.storage.getItem('token');
			data = data ||{};



			return axios.get(url, {
				params: data,
				headers: {'Authorization': token}
			}).then(APP.success)
				.catch(APP.error);
		},
		/**
		 * axios.post 
		 */
		post: function(url, data) {
			var token = plus.storage.getItem('token');
			data = data ||{};		
			return axios.post(url, data, {
				headers: {'Authorization': token,'app_type':'DMNT'}
			}).then(APP.success)
				.catch(APP.error);
		},
		paramsStrSort:function(paramsStr, kAppSecret){
//	        console.log('paramsStr***'+paramsStr)
	      var url = paramsStr
	        var urlStr = url.split("&").sort().join("&");
////	        var newUrl = urlStr + '&key=' + kAppSecret;
//	        console.log("urlStr:"+urlStr)
	        return CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(urlStr, kAppSecret)).toUpperCase();
	    },
	    getSign:function(params, kAppSecret){
	      if (typeof params == "string") {
	            return APP.paramsStrSort(params, kAppSecret);
	        } else if (typeof params == "object") {
	            var arr = [];
	            for (var i in params) {
	                arr.push((i + "=" + params[i]));
	            }
	            return APP.paramsStrSort(arr.join(("&")), kAppSecret);
	        }
	    },
		postencrypt: function(url, data, success, error) {
			
			var token = plus.storage.getItem('token') || '';
			data = data ||{};
		
			var encrypt = new JSEncrypt();
      encrypt.setPublicKey(publicKey);
      var encrypted = encrypt.encrypt(JSON.stringify(data));

      
			return axios.post(url, {encryption_parameters: encrypted}, {
				headers: {'Authorization': token}
			}).then(APP.success)
				.catch(APP.error);
		},
		/**
		 * axios.put 
		 */	
		 put:function(url,data) {
		 	var token = plus.storage.getItem('token');
			data = data ||{};
			return axios.put(url, data, {
				headers: {'Authorization': token}
			}).then(APP.success)
				.catch(APP.error);
		 },
		  patch:function(url,data) {
		 	var token = plus.storage.getItem('token');
			data = data ||{};
			return axios.patch(url, {
				headers: {'Authorization': token}
			}).then(APP.success)
				.catch(APP.error);
		 },
		 /**
		 * axios.delete 
		 */	
		 delete:function(url,data) {;
		 	var token = plus.storage.getItem('token');
			data = data ||{};
			return axios.delete(url, {
				headers: {'Authorization': token}
			}).then(APP.success)
				.catch(APP.error);
		 },
		/**
		 * 同时设置个setItem
		 */
		localStorage: {
			setItems: function(obj) {
				obj = obj || {};
				for(var key in obj) {
					localStorage.setItem(key, obj[key]);
				}
			}
		},
		/**
		 * 密码处理 
		 */
		encryPass: function(pass) {
//			return md5(pass);
			return pass;
		},
		
		
		getFisrstLogin:function(){
			var date =new Date()
			var currentTime = "" + date.getFullYear() + (date.getMonth() + 1) + date.getDate();
			
			var currentMobie	=	plus.storage.getItem('mobileAccount')
			var storeMobile = plus.storage.getItem(currentMobie);
//			if (storeMobile  === null) {
//				plus.storage.setItem(currentMobie,currentTime)
//				return true; // 要显示红包
//			
//			} else {
				if (currentTime != storeMobile) {
					plus.storage.setItem(currentMobie,currentTime);
					console.log("currentTime:"+currentTime)
					console.log("storeMobile:"+storeMobile)
					return  true;
				}
				
//			}
			return false;
			
		},
		isTenMutiple:function(number) {
			if(number==0){
				return false;
			}
			var isTen = number % 10;
			if(isTen == 0) {
				return true;
			} else {
				return false;
			}
		},
		isTwoMutiple:function(number) {
			var isTen = number % 20;
			if(isTen == 0) {
				return true;
			} else {
				return false;
			}
		},
		/**
		 * alert 
		 */
		alert: function(message, callback) {
			var btnArray = vm.lang.GOOGLE_zhidao;
			mui.alert(message, ' ', btnArray, callback);
		},
		/**
		 * confirm 
		 */
		confirm: function(message,prompt,btnArray, callback){
			var btnArray = [vm.lang.G_quudin,vm.lang.I_quxiao];
			mui.confirm(message,prompt,btnArray, callback);
		},
		/**
		 * 时间转换 
		 */
		timeTostr: function(str, ymd) {
			var NOW = new Date(str);
			var year = NOW.getFullYear();
			var month = NOW.getMonth() + 1;
			var day = NOW.getDate();
			var hours = NOW.getHours();
			var minitues = NOW.getMinutes();
			var seconds = NOW.getSeconds();
			month = month < 10 ? '0' + month : month;
			day = day < 10 ? '0' + day : day;
			hours = hours < 10 ? '0' + hours : hours;
			minitues = minitues < 10 ? '0' + minitues : minitues;
			seconds = seconds < 10 ? '0' + seconds : seconds;
			if(ymd) return year + '-' + month + '-' + day;
			return year + '-' + month + '-' + day + ' ' + hours + ':' + minitues + ':' + seconds;
		},
		loading: {
			show: function() {
				if(document.querySelector('.loading-wrap')) {
					return;
				}
				var $wrap = document.createElement('div');
				$wrap.className = 'loading-wrap';
				$wrap.innerHTML = '<div class="loading-img"><div class="img-pos"></div></div>';
				document.body.appendChild($wrap);
			},
			hide: function() {
				if(document.querySelector('.loading-wrap')) {
					document.body.removeChild(document.querySelector('.loading-wrap'));
				}
			}
		}
	};
	
	return app;
}(mui));