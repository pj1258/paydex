
var getLang = function(where,language,langcallback){
	var rtlang='';
	var parent = '';
	if(where == 2){
		parent = '../../';
	}else if(where == 3){
		parent = '../../../';
	}
	mui.getJSON(parent+'Lang/'+language+'.txt',{},function(result){
			langcallback(result);
		}
	);		
}
var LANG = {
	getLocal: function() {
		
		var language = plus.storage.getItem('lang');
		var osLang = plus.os.language;
		if(language) {
			return language;
		}
		if(osLang == 'zh_CN' || osLang == 'zh-Hans-CN'){
			language = 'zh-cn';
	 }else if(osLang == 'en_US' || osLang == 'en-CN' || osLang == 'en'){
			language = 'en-us';
	 }else if(osLang == 'zh_TW' || osLang == 'zh-Hant-CN'){
			language = 'zh-hant';
	}else if(osLang == 'ja_JP' || osLang == 'ja-CN'){
			language = 'ja-jp';
		}
	else	if(osLang == 'fr_FR' || osLang == 'fr-CN'){
			language = 'fr-fr';
		}
	else	if(osLang == 'ko_KR' || osLang == 'ko-CN'){
			language = 'ko-kr';
		}
	else	if(osLang == 'ru_RU' || osLang == 'ru-CN'){
			language = 'ru-ru';
		}
	else	if(osLang == 'vi_VN' || osLang == 'vi-CN'){
			language = 'vi-vn';
		}
	else	if(osLang == 'th_TH' || osLang == 'th-CN'){
			language = 'th-th';
		}
	else	if(osLang == 'pt_PT' || osLang == 'pt-PT'){
			language = 'pt-pt';
		}
	else	if(osLang == 'es_ES' || osLang == 'es-CN'){
			language = 'es-es';
		}else{
	  	language = 'zh-cn';
	  	
	  }
		return language;
	  plus.storage.setItem('lang',language)
		
	},
	/**
	 * 初始化语言环境， 默认zh，从localStorage里获取
	 */
	init: function(where, callback) {
		LANG.getLang(where, callback);
	},
	/**
	 * 重置当前语言，从localStore里获取 
	 */
	reset: function(where, callback) {
		LANG.getLang(where, callback);
	},
	getLang: function(where, langcallback) {
		var parent = '';
		if(where === 2){
			parent = '../../';
		}else if(where === 3){
			parent = '../../../';
		}
		var language = LANG.getLocal() || 'zh-cn';
		console.log(language)
		mui.getJSON(parent + 'Lang/' + language + '.json', {}, function(result){
			langcallback(result);
		});
	}
}
