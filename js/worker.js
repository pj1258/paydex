importScripts('app-worker.js', 'promise-6.1.0.js', 'axios.min.js');
var timer1 = timer2 = timer3 = null;

self.onmessage = function(event) {
	var data = event.data;
	switch(data.cmd) {
		case 'get-all':
			timer1 = null;
			timer2 = null;
			getAllInterval(data.token);
			fresh(data.token);
			updateTimeInterval();
			break;
		case 'stop-all':
			timer1 = true;
			timer2 = true;
			clearInterval(timer3);
			break;
		case 'get-notice':

	}
}
function updateTimeInterval() {
	timer3 = setInterval(function() {
		self.postMessage({cmd: 'update-time', payload: {}});
	}, 1000)
}

function getAllInterval(token) {
	intervalRun(token);
}
// 获取区块高度
function get_latest_all_blocks(token) {
	return new Promise(function(resolve, reject) {
		APP.get(APP.PYTHON_ROOT + '/mainchain/basic/get_latest_all_blocks', {
			order:'desc',
			page_num:'now',
			page_size:5
		}, token)
		.then(function(res){
				resolve(res);
		})
	})
}
// 获取最新事物
function get_latest_transaction_info(token) {
	return new Promise(function(resolve, reject) {
		APP.get(APP.PYTHON_ROOT + '/mainchain/basic/get_latest_transaction_info', {
			order:'desc',
			page_num:'now',
			page_size:5
		}, token)
		.then(function(res){
			resolve(res);
		});
	})
}

// 获取最新操作
function get_latest_all_operations(token) {
	return new Promise(function(resolve, reject) {
		APP.get(APP.PYTHON_ROOT + '/mainchain/basic/get_latest_all_operations', {
			order:'desc',
			page_num:'now',
			page_size:5
		}, token).then(function(res){
			resolve(res);
		});
	})
}

// 获取节点列表
function get_node(token) {
	return new Promise(function(resolve, reject) {
		APP.get(APP.PHP_ROOT + '/node', {}, token)
		.then(function(res){
			resolve(res);
		});
	})
}

function intervalRun(token) {
	Promise.all([
		get_latest_all_blocks(token), 
		get_latest_transaction_info(token), 
		get_latest_all_operations(token), 
		get_node(token)])
	.then(function (result) {
		self.postMessage({cmd: 'get-all', payload: result});
		setTimeout(function() {
			if(!timer1) {
				intervalRun(token);
			}
		}, 300000)
	}).catch(function(error) {
	  setTimeout(function() {
		  if(!timer1) {
				intervalRun(token);
			}
		}, 300000)
	})
}

function fresh(token){
	APP.get(APP.PYTHON_ROOT + '/mainchain/basic/get_main_chain_info', {}, token)
		.then(function(res){
			self.postMessage({cmd: 'get-fresh', payload: res});
			setTimeout(function() {
				if(!timer2) {
					fresh(token);
				}
			}, 1000)
		}).catch(function() {
			setTimeout(function() {
			  if(!timer2) {
					fresh(token);
				}
			}, 1000)
		})
}

