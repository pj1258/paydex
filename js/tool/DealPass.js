/*
 * DealPass||v.0.1
 * 封装DealPass动作
 * type表示使用后动作的模式
 * 		0=直接返回输入的数值
 * 		1=解密私钥后返回
 * 		2=解密私钥后提交返回结果
 * id为使用此插件的页面id
 * data表示传入的参数
 * 
 * */
if(window.plus){
	var dealpassmask = mui.createMask(_closeDealpass);
	var showdealPass = false;
	function DealPass(type,id,url,data){
//      showdealPass = true;
//      showdealPass = true;
		if(url == 0){url='./'}
		if(url == 1){url='../'}
		if(url == 2){url='../../'}
		if(url == 3){url='../../../'}
		mui.openWindow({
			url:url+'dealPass.html',
			id:'dealPass',
			show:{ 
				aniShow:'slide-in-bottom',
	      	duration:120
		  },
			waiting:{autoShow:false},
			styles:{
				height:'73%',
				bottom:0,
				scrollIndicator: 'none',//不显示滚动条
				background: 'transparent'
			},
			extras:{
				type:type,
				useid:id,
				usedata:data
			}
		});
		showdealPass = true;
		dealpassmask.show();
	}
	/**
	 * 关闭Dealpass菜单(业务部分)
	 */
	function _closeDealpass() {
		if (showdealPass) {
			//解决android 4.4以下版本webview移动时，导致fixed定位元素错乱的bug;
			if (mui.os.android && parseFloat(mui.os.version) < 4.4) {
				document.querySelector("header.mui-bar").style.position = "fixed";
				//同时需要修改以下.mui-contnt的padding-top，否则会多出空白；
				document.querySelector(".mui-bar-nav~.mui-content").style.paddingTop = "44px";
			}
	
			if(plus.webview.getWebviewById('dealPass')){
				plus.webview.getWebviewById('dealPass').close();
			}
			
			showdealPass = false;
			//关闭父级导航遮罩
		}
	}
	
	function closeDealpass() {
		//窗体移动
		_closeDealpass();
		//关闭遮罩
		dealpassmask.close();
	}
}else{// 兼容老版本的plusready事件
    document.addEventListener('plusReady',function () {
        // 在这里调用5+ API
    },false);
}