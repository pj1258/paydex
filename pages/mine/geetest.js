var GEETEST = {
	el: '',
	captcha: {},
	hooks: {},
	gt_token: null,
	lang: 'zh-cn',
	/**
	 * @params {} 对象参数
	 * @params.el 需要绑定到的元素 '#id'
	 * @params.url 验证码生成地址
	 * @params.before function  在进行极验验证之前，需要做的操作，一般是参数校验
	 * @params.ready function  onReady
	 * @params.success function  onSuccess
	 * @return {[type]} [description]
	 */
	
	init: function(params) {
		if(window.plus) {
		if(plus.networkinfo.getCurrentType() === plus.networkinfo.CONNECTION_NONE) {
//						mui.toast(vm.$t('lang.COM_wangluolianjieshibai'));The network link failed, please check your network!
            mui.toast('The network link failed, please check your network!');
			return false;
		}
	}
		if(!params.el) {
			throw new Error('must be set "el"');
		}
		if(!params.success) {
			throw new Error('must be set "success"');
		}
		GEETEST.lang = params.lang || 'zh-cn';
		GEETEST.el = params.el;
		GEETEST.hooks.before = params.before;
		GEETEST.hooks.ready = params.ready;
		GEETEST.hooks.success = params.success;

		var url = params.url || 'http://47.244.140.126:8883/api/jYParam';
		GEETEST.initGeetest(url, GEETEST.handler);
	},
	initGeetest: function(url, handler) {
		APP.post(url, {
			t: (new Date()).getTime()
		}).then(function(res) {
			console.log('就按**'+JSON.stringify(res))
			var data = res.data;
      GEETEST.gt_token = data.gt_token;
      GEETEST.offline=data.offline
      console.log("GEETEST.offline:"+GEETEST.offline)
      initGeetest({
        gt: data.gt,
        challenge: data.challenge,
        offline: !data.success, // 表示用户后台检测极验服务器是否宕机
        new_captcha: data.new_captcha, // 用于宕机时表示是新验证码的宕机
        product: "bind", // 产品形式，包括：float，popup, custom
        https: true,
        lang: GEETEST.lang
      }, GEETEST.handler);
		});
	},
	handler: function(captchaObj) {
		GEETEST.captcha = captchaObj;
		captchaObj.onReady(function () {
      GEETEST.hooks.ready && GEETEST.hooks.ready();
    })
    captchaObj.onSuccess(function () {
      var result = captchaObj.getValidate();
      result.gt_token = GEETEST.gt_token;
      result.offline = GEETEST.offline;
      GEETEST.hooks.success(result);
    });
    // 如果是ID，
    if(GEETEST.el.indexOf('#') === 0) {
	    	document.querySelector(GEETEST.el).addEventListener('click', function() {
	    		// 如果before验证，则先执行before验证
		    	if(GEETEST.hooks.before) {
		    		// 把this回传给before，在外面做判断使用
		    		if(!GEETEST.hooks.before(this)) {
		    			return;
		    		}
		    	}
		    	captchaObj.verify();
	    	});
	    	return;
    }
    var els = document.querySelectorAll(GEETEST.el);
    for(var ii = 0, ilen = els.length; ii < ilen; ii++) {
	    	els[ii].addEventListener('click', function() {
	    		// 如果before验证，则先执行before验证
		    	if(GEETEST.hooks.before) {
		    		// 把this回传给before，在外面做判断使用
		    		if(!GEETEST.hooks.before(this)) {
		    			return;
		    		}
		    	}
		    	captchaObj.verify();
	    })
    }
    
	},
	reset: function() {
		GEETEST.captcha.reset();
	}
}
